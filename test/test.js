const { assert } = require('chai');
const { newUser } = require('../index.js');

// describe gives structure to your test suite
describe('Test newUser Object', () => {

	// unit test
	it('Assert newUser type is object', () => {

		assert.equal(typeof(newUser),'object');

	});

	it('Assert newUser.email is type string', () => {

		assert.equal(typeof(newUser.email),'string');

	});

	// .equal = testing for equality ==
	it('Asser newUSer.email is not undefined', () => {

		assert.notEqual(typeof(newUser.email),undefined);

	});

	// .notEqual = testing for inequality !=

	it('Assert newUser.password is a string',() => {

		assert.equal(typeof(newUser.password),'string');

	});

	it('Assert that the number of characters in newUser.password is atleast 16',() => {

		// .isAtLeast = testing for greater than or equal to >=
		assert.isAtLeast(newUser.password.length,16);

	});

	// Mini-Activity:
	// Create a test that assers that newUser.email is NOT equal to undefined.

	// Activity

	// Soln 1
	it('Assert that the newUser firstName type is a string',() =>{

		assert.equal(typeof(newUser.firstName),'string');

	});

	// Soln 2
	it('Assert that the newUser lastName type is a string',() =>{

		assert.equal(typeof(newUser.lastName),'string');

	});

	// Soln 3
	it('Assert that the newUser firstName type is not undefined',() =>{

		assert.notEqual(typeof(newUser.firstName),'undefined');

	});

	// Soln 4
	it('Assert that the newUser lastName type is not undefined',() =>{

		assert.notEqual(typeof(newUser.lastName),'undefined');

	});

	// Soln 5
	it('Assert that the newUser age is atleast 18',() =>{

		assert.isAtLeast(newUser.age,18);

	});

	// Soln 6
	it('Assert that the newUser age type is a number',() =>{

		assert.equal(typeof(newUser.age),'number');

	});

	// Soln 7
	it('Assert that newUser contact number type is a string',() =>{

		assert.equal(typeof(newUser.contactNumber),'string');

	});

	// Soln 8
	it('Assert that newUser contact number is atleast 11 characters',() =>{

		assert.isAtLeast(newUser.contactNumber.length,11);

	});

	// Soln 9
	it('Assert that the newUser batch number type is a number',() =>{

		assert.equal(typeof(newUser.batchNumber),'number');

	});

	// Soln 10
	it('Assert that newUser batch number is not undefined',() =>{

		assert.notEqual(typeof(newUser.batchNumber),'undefined');

	});






});