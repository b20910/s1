const newUser = {

	firstName: 'John',
	lastName: 'Dela Cruz',
	age: 18,
	contactNumber: '09266772411',
	batchNumber: 209,
	email: 'johnDelaCruz@gmail.com',
	password: 'sixteencharacters'

};

module.exports = {
	newUser: newUser
}

/*
	Activity Instructions:

	1. Assert that the newUser firstName type is a string
	2. Assert that the newUser lastName type is a string
	3. Assert that the newUser firstName type is not undefined
	4. Assert that the newUser lastName type is not undefined
	5. Assert that the newUser age is atleast 18
	6. Assert that the newUser age type is a number
	7. Assert that newUser contact number type is a string
	8. Assert that newUser contact number is atleast 11 characters
	9. Assert that newUser batch number type is a number
	10. Assert that newUser batch number is not undefined.
*/